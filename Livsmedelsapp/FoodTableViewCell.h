//
//  FoodTableViewCell.h
//  Livsmedelsapp
//
//  Created by ITHS on 2016-03-17.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cellLabel;

@end

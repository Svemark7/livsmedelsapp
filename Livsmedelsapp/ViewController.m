//
//  ViewController.m
//  Livsmedelsapp
//
//  Created by ITHS on 2016-03-08.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "ViewController.h"
#import "TableViewController.h"

@interface ViewController ()
@property (nonatomic) NSMutableArray *food;
@property (weak, nonatomic) IBOutlet UITextField *foodKey;
@property (weak, nonatomic) IBOutlet UIImageView *potato;
@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.food = [[NSMutableArray alloc] init].mutableCopy;
    
    self.potato.transform = CGAffineTransformMakeRotation(0.7);
    self.animator = [[UIDynamicAnimator alloc]initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.potato]];
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[self.potato]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    
    [self.animator addBehavior:self.gravity];
    [self.animator addBehavior:self.collision];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onTap:(UITapGestureRecognizer*)sender {
    
    self.potato.center = [sender locationInView:self.view];
    self.potato.transform = CGAffineTransformMakeRotation(0.8);
    
    self.animator = [[UIDynamicAnimator alloc]initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.potato]];
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[self.potato]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    
    [self.animator addBehavior:self.gravity];
    [self.animator addBehavior:self.collision];
}

- (IBAction)search:(id)sender {
    
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff?query=%@", self.foodKey.text];
    
    NSString *escaped = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSURL *url = [NSURL URLWithString:escaped];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
        
        if(error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *jsonParseError = nil;
        
        NSMutableArray *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        
        if(jsonParseError) {
            NSLog(@"Failed to parse data: %@", jsonParseError);
            return;
        }
        
     //   NSLog(@"Result: %@", result);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(result.count<=0) {
            } else {
                self.food = result;
                NSLog(@"Result: %@", self.food);
            }
            
        });
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"Result" sender:self];
        });
        
    }];
    
    [task resume];
    
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    TableViewController *tableView = [segue destinationViewController];
    tableView.foodList = self.food;
}

@end

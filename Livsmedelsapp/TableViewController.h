//
//  TableViewController.h
//  Livsmedelsapp
//
//  Created by ITHS on 2016-02-29.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController

@property (nonatomic) NSMutableArray *foodList;

@end

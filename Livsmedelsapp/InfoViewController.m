//
//  InfoViewController.m
//  Livsmedelsapp
//
//  Created by ITHS on 2016-03-04.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "InfoViewController.h"

@interface InfoViewController ()
@property (weak, nonatomic) IBOutlet UILabel *protein;
@property (weak, nonatomic) IBOutlet UILabel *fett;
@property (weak, nonatomic) IBOutlet UILabel *vitaminB12;
@property (weak, nonatomic) IBOutlet UILabel *vitaminB6;
@property (weak, nonatomic) IBOutlet UILabel *vitaminC;
@property (weak, nonatomic) IBOutlet UILabel *vitaminD;
@property (weak, nonatomic) IBOutlet UILabel *vitaminE;
@property (weak, nonatomic) IBOutlet UILabel *vitaminK;
@property (weak, nonatomic) IBOutlet UILabel *goodValue;
@property (weak, nonatomic) IBOutlet UILabel *goodValueText;

@property (nonatomic) NSMutableDictionary *nutrients;

@end

@implementation InfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.nutrients = [[NSMutableDictionary alloc] init].mutableCopy;
    
    [self getInfo];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelay:0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    self.goodValueText.center = CGPointMake(self.goodValueText.center.y, 30);
    self.goodValue.center = CGPointMake(self.goodValue.center.y, 500);
    [UIView commitAnimations];
    
    NSLog(@"%@", self.number);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getInfo {
    
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", self.number];
    
    NSString *escaped = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSURL *url = [NSURL URLWithString:escaped];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
        
        if(error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *jsonParseError = nil;
        
        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        
        if(jsonParseError) {
            NSLog(@"Failed to parse data: %@", jsonParseError);
            return;
        }
        
        self.nutrients = [result objectForKey:@"nutrientValues"];
        
        self.title = [result objectForKey:@"name"];
    
        NSLog(@"Result: %@", self.nutrients);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(result.count<=0) {
            } else {
                self.protein.text = [NSString stringWithFormat:@"%@", [self.nutrients objectForKey:@"protein"]];
                self.fett.text = [NSString stringWithFormat:@"%@", [self.nutrients objectForKey:@"fat"]];
                self.vitaminB12.text = [NSString stringWithFormat:@"%@", [self.nutrients objectForKey:@"energyKcal"]];
                self.vitaminB6.text = [NSString stringWithFormat:@"%@", [self.nutrients objectForKey:@"carbohydrates"]];
                self.vitaminC.text = [NSString stringWithFormat:@"%@", [self.nutrients objectForKey:@"vitaminC"]];
                self.vitaminD.text = [NSString stringWithFormat:@"%@", [self.nutrients objectForKey:@"cholesterol"]];
                self.vitaminE.text = [NSString stringWithFormat:@"%@", [self.nutrients objectForKey:@"iron"]];
                self.vitaminK.text = [NSString stringWithFormat:@"%@", [self.nutrients objectForKey:@"zink"]];
        
                int fat = [[self.nutrients objectForKey:@"fat"] intValue];
                int protein = [[self.nutrients objectForKey:@"protein"] intValue];
                int goodValue = fat * protein;
                self.goodValue.text = [NSString stringWithFormat:@"%d", goodValue];
            }
            
        });
    }];
    
    [task resume];

}

@end
